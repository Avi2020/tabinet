#include "Game.hpp"
#define WAIT_TIME 2
Game::Game() : m_lastPlayerUsedACard(m_player1)
{
    m_mainDeck.InitializeMainDeck();
}
void Game::InitializePlayersName()
{
    std::string name;
    std::cout << "Player 1 name:\n";
    std::cin >> name;
    //name = "dorel";
    m_player1 = Player(name);
    std::cout << "Player 2 name:\n";
    std::cin >> name;
    //name = "mirel";
    m_player2 = Player(name);
    if (m_player1 == m_player2)
    {
        InitializePlayersName();
    }
}
void Game::FirstDeckAddPlusConfirm()
{
    bool confirm = false;
    Deck::FillDeck(m_player1.GetDeck(), m_mainDeck, 4);
    std::cout << "first hand of cards are:\n";
    std::cout << m_player1.GetDeck() << "\n";
    std::cout << "Wanna keep this cards?(1 yes | 0 no)\n";
    std::cin >> confirm;
    if (confirm)
    {
        Deck::FillDeck(m_player1.GetDeck(), m_mainDeck, 2);
        Deck::FillDeck(m_tableDeck, m_mainDeck, 4);
    }
    else
    {
        m_tableDeck = m_player1.GetDeck();
        m_player1.GetDeck().GetCards().clear();
        Deck::FillDeck(m_player1.GetDeck(), m_mainDeck, 6);
    }
    Deck::FillDeck(m_player2.GetDeck(),m_mainDeck,6);
    std::cout << "Table deck is:" << m_tableDeck << '\n';
}
void Game::Turn(Player &player, Player &enemy)
{
    if (m_tableDeck.IsEmpty())
    {
        Deck::FillDeck(m_tableDeck, m_mainDeck, 4);
    }
    TurnOption(player, enemy);

    if (player.GetDeck().IsEmpty())
    {

            if (player == m_player2)
            {
                for(int i=0;i<6;i++)
                {
                Deck::FillDeck(player.GetDeck(), m_mainDeck, 1);
                Deck::FillDeck(enemy.GetDeck(), m_mainDeck, 1);
                }
            } 
        
        
        if ((enemy.GetDeck().IsEmpty()) && (m_mainDeck.IsEmpty()))
        {
            FinalCalculations();
            return;
        }
    }
}

void Game::Discard(Player &player, Player &enemy)
{
    ClearScreen();
    std::cout << "----------------------------\n"
              << player.GetName() << "'s Turn! :)\n"
              << "----------------------------\n"
              << player.GetName() << "'s Deck:\n"
              << player.GetDeck() << '\n'
              << "----------------------------\n"
              << "Table Deck is:\n"
              << m_tableDeck << '\n'
              << "----------------------------\n"
              << "---DISCARD MENU---\n"
              << "options: 0 <-Exit this menu\n"
              << "------ 1-" << player.GetDeck().DeckSize() << " <-Discard The Card by number\n"
              << "------- -1 <-Exit the Game\n";
    int option;
    std::cin >> option;
    switch (option)
    {
    case 0:
        TurnOption(player, enemy);
        break;
    case -1:
        exit(0);
        break;
    default:
        if (option > player.GetDeck().DeckSize())
        {
            ClearScreen();
            std::cout << "!!!!!!wrong Option!!!!!!!!\n";
            std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
            ClearScreen();
            Discard(player, enemy);
        }
        else
        {
            m_tableDeck.GetCards().push_back(player.GetDeck().GetCards().at(option - 1));
            std::cout << player.GetDeck().GetCards().at(option - 1) << " Discarded!\n";
            std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
            player.GetDeck().RemoveCard(option);
        }
        break;
    }
}
void Game::FinalCalculations()
{
    if (PlayerPoints(m_player1, m_player2) > PlayerPoints(m_player2, m_player1))
    {
        std::cout << m_player1.GetName()
                  << " WIN! \n---> Number of Points: "
                  << m_player1.GetPoints() << " <---\n"
                  << "enemy points: " << m_player2.GetPoints();
        std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
        exit(0);
    }
    else if (PlayerPoints(m_player1, m_player2) < PlayerPoints(m_player2, m_player1))
    {
        std::cout << m_player2.GetName()
                  << " WIN! \n---> Number of Points: "
                  << m_player2.GetPoints() << " <---\n"
                  << "enemy points: " << m_player1.GetPoints();
        std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
        exit(0);
    }
    else
    {
        std::cout << "IT'S A TIE!!!!!";
        std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
        exit(0);
    }
}
int Game::PlayerPoints(Player &player, Player &enemy)
{
    player.AddPoints(player.GetTables());
    if (m_lastPlayerUsedACard.GetName() == player.GetName())
    {
        Deck::FillDeck(player.GetCardsColected(), m_tableDeck, m_tableDeck.DeckSize());
    }
    for (auto card : player.GetCardsColected().GetCards())
    {
        if ((card.GetValue() == Card::CardValue::ace) ||
            (card.GetValue() == Card::CardValue::jack) ||
            (card.GetValue() == Card::CardValue::king) ||
            (card.GetValue() == Card::CardValue::queen) ||
            (card.GetValue() == Card::CardValue::ten))
        {
            player.AddPoints(1);
        }
        if ((card.GetValue() == Card::CardValue::ten) &&
            (card.GetType() == Card::CardType::Diamonds))
        {
            player.AddPoints(1);
        }
        if ((card.GetValue() == Card::CardValue::two) &&
            (card.GetType() == Card::CardType::Clubs))
        {
            player.AddPoints(1);
        }
        if (card.GetType() == Card::CardType::Clubs)
        {
            player.AddPoints(1);
        }
    }
    if (player.GetCardsColected().DeckSize() > enemy.GetCardsColected().DeckSize())
    {
        player.AddPoints(3);
    }
    return player.GetPoints();
}

void Game::TurnOption(Player &player, Player &enemy)
{
    ClearScreen();
    std::cout << "----------------------------\n"
              << player.GetName() << "'s Turn! :)\n"
              << "----------------------------\n"
              << player.GetName() << "'s Deck:\n"
              << player.GetDeck() << '\n'
              << "----------------------------\n"
              << "Table Deck is:\n"
              << m_tableDeck << '\n'
              << "----------------------------\n"
              << "options: 0 <-Discard\n"
              << "------ 1-" << player.GetDeck().DeckSize() << " <-Use the card by number\n"
              << "------- -1 <-Exit the Game\n";

    int option;
    std::cin >> option;
    switch (option)
    {
    case 0:
        Discard(player, enemy);
        break;
    case -1:
        exit(0);
        break;
    default:
        if (option > player.GetDeck().DeckSize())
        {

            std::cout << "!!!!!!wrong Option!!!!!!!!\n";
            std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
            ClearScreen();
            TurnOption(player, enemy);
        }
        else
        {
            m_backupDeck = m_tableDeck;
            UseCard(player, enemy, option);
        }
        break;
    }
}
void Game::UseCard(Player &player, Player &enemy, const int &cardId, int stored, const int &pairSize)
{
    auto &usedCard = player.GetDeck().GetCards().at(cardId - 1);
    int16_t valueRemained = Card::ReturnInGameCardValue(usedCard) - stored;
    ClearScreen();
    if (valueRemained == 0)
    {
        for (auto i : m_backupDeck.GetCards())
        {
            bool isThere = false;
            for (auto j : m_tableDeck.GetCards())
            {
                if (i==j)
                {
                    isThere = true;
                }
            }
            if (!isThere)
            {
                player.GetCardsColected().AddCard(i);
            }
        }
        player.GetCardsColected().AddCard(usedCard);
        player.GetDeck().RemoveCard(usedCard);
        std::cout << ">!<Cards Colected:" << player.GetCardsColected() << ">!<\n";
        if (m_tableDeck.IsEmpty())
        {
            if (enemy.GetTables() > 0)
            {
                enemy.SetTables(enemy.GetTables() - 1);
            }
            else
            {
                player.SetTables(player.GetTables() + 1);
            }
        }
        m_lastPlayerUsedACard = player;
        return;
    }
    else if (valueRemained < 0)
    {
        m_tableDeck = m_backupDeck;
        TurnOption(player, enemy);
        return;
    }
    if (pairSize == 0)
    {
        m_tableDeck = m_backupDeck;
        TurnOption(player, enemy);
        return;
    }
    std::cout << "----------------------------\n"
              << "Table Deck is:\n"
              << m_tableDeck << '\n'
              << "----------------------------\n"
              << "You Need To Make The Value:" << valueRemained << '\n'
              << "Cards you can draw: " << pairSize << '\n'
              << "----------------------------\n"
              << "options: 0 <-Cancel\n"
              << "------ 1-" << m_tableDeck.DeckSize() << " <-Use the card by number\n"
              << "------- -1 <-Exit the Game\n";

    int option;
    std::cin >> option;
    switch (option)
    {
    case 0:
    {
        m_tableDeck = m_backupDeck;
        TurnOption(player, enemy);
    }
    break;
    case -1:
        exit(0);
        break;
    default:
        if (option > m_tableDeck.DeckSize())
        {

            std::cout << "!!!!!!wrong Option!!!!!!!!\n";
            std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
            ClearScreen();
            UseCard(player, enemy, cardId, stored, pairSize);
        }
        else
        {
            stored += Card::ReturnInGameCardValue(m_tableDeck.GetCards().at(option - 1));
            m_tableDeck.RemoveCard(option);

            UseCard(player, enemy, cardId, stored, pairSize - 1);
        }
        break;
    }
}
void Game::Run()
{
    ClearScreen();
    InitializePlayersName();
    FirstDeckAddPlusConfirm();
    while (true)
    {
        Turn(m_player1, m_player2);
        Turn(m_player2, m_player1);
    }
}

void Game::ClearScreen()
{
#ifdef OSisWindows

    system("cls");
#else
    system("clear");
#endif
}