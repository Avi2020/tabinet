﻿#include "Card.hpp"

Card::Card()
{
    m_type = Card::CardType::UNKNOWN;
    m_value = Card::CardValue::_UNKNOWN;
}
Card::Card(const int &value, const int &type)
{
    m_type = static_cast<CardType>(type);
    m_value = static_cast<CardValue>(value);
}
Card::CardType &Card::GetType()
{
    return m_type;
}
Card::CardValue &Card::GetValue()
{
    return m_value;
}

void Card::SetType(const CardType &type)
{
    m_type = type;
}
void Card::SetType(const int &type)
{
    m_type = static_cast<CardType>(type);
}
void Card::SetValue(const CardValue &value)
{
    m_value = value;
}
void Card::SetValue(const int &value)
{
    m_value = static_cast<CardValue>(value);
}
Card &Card::operator=(const Card &other)
{
    m_type = other.m_type;
    m_value = other.m_value;
    return *this;
}


std::ostream &operator<<(std::ostream &os, const Card &card)
{
    switch (card.m_value)
    {
    case Card::CardValue::ace:
        os << "A";
        break;
    case Card::CardValue::jack:
        os << "J";
        break;
    case Card::CardValue::queen:
        os << "Q";
        break;

    case Card::CardValue::king:
        os << "K";
        break;

    default:
        os << (int16_t)card.m_value;
        break;
    }
    switch (card.m_type)
    {
    case Card::CardType::Clubs:
        os << "♣";
        //os << "T";
        break;
    case Card::CardType::Diamonds:
        os << "♦";
        //os << "R";
        break;
    case Card::CardType::Hearts:
        os << "♥";
        //os << "IR";
        break;

    case Card::CardType::Spades:
        os << "♠";
        //os << "IN";
        break;

    default:
        break;
    }

    return os;
}
int Card::ReturnInGameCardValue(const Card& card)
{
    if(card.m_value==Card::CardValue::ace)
    {
    return 11;
    }
    if(card.m_value>=Card::CardValue::jack)
    {
        return (int16_t)card.m_value+1;
    }
    return (int16_t)card.m_value;
}
bool operator==(const Card& i,const Card& j)
{
    if((i.m_value == j.m_value) && (i.m_type == j.m_type))
    {
        return true;
    }
    return false;
}