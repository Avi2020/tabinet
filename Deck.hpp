
#pragma once
#include "Card.hpp"
#include <vector>
#include <iostream>
#include <algorithm>
#include <random>
#include<chrono>

class Deck
{
protected:
	std::vector<Card> m_cards;

public:
	int16_t DeckSize();
	bool IsEmpty();
	std::vector<Card> &GetCards();
	void AddCard(const Card &card);
	void RemoveCard(const int &cardPos);
	void RemoveCard(Card &card);
	Card ObtainLastCard();
	void ShowDeck();
	static void FillDeck(Deck& to ,Deck& from,int howmuch);
	Deck();

	friend std::ostream &operator<<(std::ostream &os, const Deck &cards);
};
class MainDeck : public Deck
{
public:

	void InitializeMainDeck();
};