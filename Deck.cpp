#include "Deck.hpp"

void MainDeck::InitializeMainDeck()
{
    for (int16_t i = 1; i <= CARD_TYPE_SIZE; i++)
    {
        for (int16_t j = 1; j <= CARD_VALUE_SIZE; j++)
        {
            m_cards.push_back(Card(j, i));
        }
    }
    srand(std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count()
    );
    auto rng = std::default_random_engine(rand());
    std::shuffle(m_cards.begin(), m_cards.end(), rng);
}


bool Deck::IsEmpty()
{
    if (m_cards.size()==0)
    {
        return true;
    }
    else 
    { 
        return false;
    }
}

int16_t Deck::DeckSize()
{
    return m_cards.size();
}

std::vector<Card> &Deck::GetCards()
{
    return m_cards;
}

void Deck::AddCard(const Card &card)
{
    m_cards.push_back(card);
}

void Deck::RemoveCard(const int &cardPos)
{
    if (cardPos <= m_cards.size())
        m_cards.erase(m_cards.begin() + cardPos - 1);
    else
        std::cout << "[Card doesn't Exist!]\n";
}

void Deck::RemoveCard(Card &card)
{

    for (int i = 0; i < m_cards.size(); i++)
    {
        if (m_cards[i]== card)
        {
            m_cards.erase(m_cards.begin() + i);
            return;
        }
    }
    std::cout << "[Card doesn't Exist!]\n";
}
Card Deck::ObtainLastCard()
{
    if (m_cards.size() > 0)
    {
        Card aux = m_cards[m_cards.size() - 1];
        m_cards.pop_back();
        return aux;
    }
    else
    {
        std::cout << "[this Deck Is Out Of Cards]\n";
        return Card(0, 0);
    }
}

void Deck::ShowDeck()
{
    for (auto i : m_cards)
    {
        std::cout << i << "\n";
    }
}
std::ostream &operator<<(std::ostream &os, const Deck &cards)
{
    for(auto card:cards.m_cards)
    {
        os<<card<<" ";
    }
    return os;
}
void Deck::FillDeck(Deck& to ,Deck& from,int howmuch)
{
    
    for(int i=0;i<howmuch;i++)
    {
        if (!from.IsEmpty()) {
            to.AddCard(from.ObtainLastCard());
        }
    }
}
Deck::Deck()
{
}
