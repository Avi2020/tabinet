#include "Player.hpp"

Player::Player() : m_points(0),
                   m_tables(0),
                   m_name("Player")
{
}
Player::Player(const std::string &name) : m_name(name),
                                          m_points(0),
                                          m_tables(0)
{
}

Player::~Player()
{
}

Deck &Player::GetDeck()
{
    return m_deck;
}
int16_t Player::GetPoints()
{
    return m_points;
}
void Player::AddPoints(const int16_t& value)
{
    m_points += value;
}
Deck &Player::GetCardsColected()
{
    return m_cardscolected;
}
int16_t Player::GetTables()
{
    return m_tables;
}

void Player::SetTables(const int16_t& tables)
{
    m_tables = tables;
}

std::string Player::GetName()
{
    return m_name;
}
bool operator==(const Player& p1,const Player& p2)
{
    if(p1.m_name==p2.m_name)
    {
        return true;
    }
    return false;
}