
#pragma once
#include "Deck.hpp"
#include <string>
class Player
{
private:
	Deck m_deck;
	Deck m_cardscolected;
	std::string m_name;
	int16_t m_points;
	int16_t m_tables;
	

public:
	Deck &GetDeck();
	int16_t GetPoints();
	void AddPoints(const int16_t& value);
	Deck &GetCardsColected();
	int16_t GetTables();
	void SetTables(const int16_t& tables);
	std::string GetName();
	Player();
	Player(const std::string &name);
	friend bool operator==(const Player& p1,const Player& p2);
	~Player();
};