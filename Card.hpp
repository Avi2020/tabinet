#include <iostream>

#pragma once

#define CARD_TYPE_SIZE 4
#define CARD_VALUE_SIZE 13
class Card
{
public:
	enum class CardType
	{
		UNKNOWN = 0,
		Clubs = 1,
		Diamonds = 2,
		Hearts = 3,
		Spades = 4

	};
	enum class CardValue
	{
		_UNKNOWN = 0,
		ace = 1,
		two = 2,
		three = 3,
		four = 4,
		five = 5,
		six = 6,
		seven = 7,
		eight = 8,
		nine = 9,
		ten = 10,
		jack = 11,
		queen = 12,
		king = 13
	};

private:
	CardType m_type;
	CardValue m_value;

public:
	friend std::ostream &operator<<(std::ostream &os, const Card &card);
	CardType &GetType();
	CardValue &GetValue();
	void SetType(const CardType &type);
	void SetType(const int &type);
	void SetValue(const CardValue &value);
	void SetValue(const int &value);
	static int ReturnInGameCardValue(const Card& card);
	Card();
	Card(const int &value, const int &type);
    friend bool operator==(const Card& i,const Card& j);
	Card &operator=(const Card &other);
};