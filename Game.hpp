
#pragma once
#include<chrono>
#include<thread>
#include "Player.hpp"
#include "Deck.hpp"
class Game
{
private:
	Player m_player1;
	Player m_player2;
	MainDeck m_mainDeck;
	Deck m_tableDeck,m_backupDeck;
	Player m_lastPlayerUsedACard;
	void ClearScreen();
public:
	void InitializePlayersName();
	void FirstDeckAddPlusConfirm();
	void Turn(Player& player, Player& enemy);
	void TurnOption(Player& player, Player& enemy);
	void UseCard(Player& player, Player& enemy,const int& cardId,int stored=0,const int& pairSize=2);
	void Discard(Player& player, Player& enemy);
	void FinalCalculations();
	int PlayerPoints( Player& player, Player& enemy);
	void Run();
	
	Game();

};